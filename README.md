# ez_country_blocker

A script that helps identify foreign traffic connected to your server and easily block any or all of the results using CSF. This tool was written for cPanel servers but will work on any Linux based server running CSF.

   Features:
       Excludes all locally configured IP addresses automatically
       Configure individual country exclusions (US,MX,CA excluded by default)
       Configure which services to scan (80,443,8080,110,995,143,993,2095,2096 by default)
       Supports Temporary or Permanent Blocking (default block duration is 1 week)
       Configurable output script target (default: /root/ez-country-blocker-block-all)
       Automatically Ignores LW Internal IP ranges. 
       Toggle for treating unknown IPs as foreign (disabled by default)
       Toggle for treating internal Private IPs as foreign (disabled by default)
       Self-Updating
       Configuration file to retain settings between updates, created automatically on first run:  /etc/ez_country_blocker.conf
